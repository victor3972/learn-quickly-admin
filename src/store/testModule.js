const INCREMENT_BY = 'INCREMENT_BY';
const test = {
  namespaced: true,
  state: () => ({
    count: 666,
  }),
  getters: {
    total: function (state) {
      return state.count * 100;
    },
  },
  mutations: {
    // increment: function (state) {},
    // 同步提交
    increment(state, payload) {
      // console.log(payload);
      // state.count += payload;
      // console.log(payload);
      // state.count += payload.amount;
      console.log(payload);
      state.count += payload;
    },
    // ['INCREMENT_BY'](state, payload) {
    //   state.count += payload;
    // },
    [INCREMENT_BY](state, payload) {
      state.count += payload;
    },
  },
  actions: {},
};
export default test;
