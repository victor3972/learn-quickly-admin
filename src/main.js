import Vue from "vue";
// 按需加载 element-ui 组件
import {
  Dropdown,
  DropdownMenu,
  DropdownItem,
  Menu,
  Submenu,
  MenuItem,

  MenuItemGroup,
  Input,
  InputNumber,
  Radio,
  RadioGroup,
  RadioButton,
  Checkbox,
  CheckboxButton,
  CheckboxGroup,
  Switch,
  Select,
  Option,
  OptionGroup,
  Button,
  ButtonGroup,
  Table,
  TableColumn,
  DatePicker,
  TimeSelect,
  TimePicker,
  Popover,
  Tooltip,
  Breadcrumb,
  BreadcrumbItem,
  Form,
  FormItem,
  Tabs,
  TabPane,
  Tag,
  Tree,
  Alert,
  Slider,
  Icon,
  Row,
  Col,
  Upload,
  Progress,
  Spinner,
  Badge,
  Card,
  Rate,
  Steps,
  Step,
  Carousel,
  CarouselItem,
  Collapse,
  CollapseItem,
  Cascader,
  ColorPicker,
  Transfer,
  Container,
  Header,
  Aside,
  Main,
  Footer,
  Timeline,
  TimelineItem,
  Link,
  Divider,
  Image,
  Calendar,
  Backtop,
  PageHeader,
  CascaderPanel,
  Loading,
  MessageBox,
  Message,
  Notification,
  Drawer,
  Dialog,
  Pagination,
} from "element-ui";
import moment from "moment";
import JsonExcel from "vue-json-excel";
// import App from "./App.vue";
// import router from "./router";
// import store from "./store";
// import ElementUI from 'element-ui';
import "element-ui/lib/theme-chalk/index.css";

Vue.component("downloadExcel", JsonExcel);


// =======
//   Table,
//   TableColumn,
//   Switch,
//   PageHeader,
//   Popover,
//   Pagination,
//   MessageBox,
//   OptionGroup,
//   Tag,
//   DatePicker,
//   Dialog,
//   Loading,
//   Drawer,
//   Select,
//   Option,
//   Tabs,
//   TabPane,
//   Image,
//   Upload,
//   Icon,  
//   Tooltip,
//   Link,
//   Cascader, 

// } from 'element-ui';
import App from './App.vue';
import router from './router';
import store from './store';

// 引入echarts
import * as echarts from 'echarts'
Vue.prototype.$echarts = echarts



// 引入初始化的样式
import "@/assets/common/reset.css";
import "@/assets/common/iconfont/iconfont.css";
import "@/assets/common/common.less";

// 引入全局自定义组件
import ThumbBar from "@/components/thumb-bar";
import backBtnAndTitle from "@/components/backBtnAndTitle";
import uploadImg from "@/components/uploadImg";
Vue.component("ThumbBar", ThumbBar);
Vue.component("backBtnAndTitle", backBtnAndTitle);
Vue.component("uploadImg", uploadImg);

// 引入请求库

// import instance from "@/api/instance";
// Vue.prototype.$request = instance;
// Vue.prototype.$message = Message;

// Vue.use(Autocomplete);

import instance from '@/api/instance';

Vue.prototype.$request = instance;
Vue.prototype.$message = Message;
Vue.component(Button.name, Button);
Vue.use(Pagination);
Vue.use(Dialog);
Vue.use(Option);
Vue.use(Select);
Vue.use(Tag);
Vue.use(Row);
Vue.use(Col);
Vue.use(Form);
Vue.use(Table);
Vue.use(TableColumn);
Vue.use(FormItem);
Vue.use(Input);
// >>>>>>> 3ee5eb0557ca4fde8b7e53fffdac0cc7813e73f3
Vue.use(Dropdown);
Vue.use(Tag);
Vue.use(DropdownMenu);
Vue.use(DropdownItem);
Vue.use(Menu);
Vue.use(Submenu);
Vue.use(MenuItem);
// <<<<<<< HEAD
Vue.use(MenuItemGroup);
Vue.use(Input);
Vue.use(InputNumber);
Vue.use(Radio);
Vue.use(RadioGroup);
Vue.use(RadioButton);
Vue.use(Checkbox);
Vue.use(CheckboxButton);
Vue.use(CheckboxGroup);
Vue.use(Switch);
Vue.use(Select);
Vue.use(Option);
Vue.use(OptionGroup);
Vue.use(Button);
Vue.use(ButtonGroup);
Vue.use(Table);
Vue.use(TableColumn);
Vue.use(DatePicker);
Vue.use(TimeSelect);
Vue.use(TimePicker);
Vue.use(Popover);
Vue.use(Tooltip);
Vue.use(Breadcrumb);
Vue.use(BreadcrumbItem);
Vue.use(Form);
Vue.use(FormItem);
Vue.use(Tabs);
Vue.use(TabPane);
Vue.use(Tag);
Vue.use(Tree);
Vue.use(Alert);
Vue.use(Slider);
Vue.use(Icon);
Vue.use(Row);
Vue.use(Col);
Vue.use(Upload);
Vue.use(Progress);
Vue.use(Spinner);
Vue.use(Badge);
Vue.use(Card);
Vue.use(Rate);
Vue.use(Steps);
Vue.use(Step);
Vue.use(Carousel);
Vue.use(CarouselItem);
Vue.use(Collapse);
Vue.use(CollapseItem);
Vue.use(Cascader);
Vue.use(ColorPicker);
Vue.use(Transfer);
Vue.use(Container);
Vue.use(Header);
Vue.use(Aside);
Vue.use(Main);
Vue.use(Footer);
Vue.use(Timeline);
Vue.use(TimelineItem);
Vue.use(Link);
Vue.use(Divider);
Vue.use(Image);
Vue.use(Calendar);
Vue.use(Backtop);
Vue.use(PageHeader);
Vue.use(CascaderPanel);
Vue.use(Drawer);
Vue.use(Loading.directive);
Vue.use(Loading);
Vue.prototype.$loading = Loading.service;
Vue.prototype.$msgbox = MessageBox;
Vue.prototype.$alert = MessageBox.alert;
Vue.prototype.$confirm = MessageBox.confirm;
Vue.prototype.$prompt = MessageBox.prompt;
Vue.prototype.$notify = Notification;
Vue.prototype.$message = Message;
Vue.config.productionTip = false;
// 定义一个时间样式过滤器
Vue.filter("dataFormat", (str, pattern = "YYYY-MM-DD HH:mm:ss") => {
  if (!str) {
    return str;
  }
  return moment(str).format(pattern);
});
Vue.filter("dataFormatNoS", (str, pattern = "YYYY-MM-DD HH:mm") => {
  if (!str) {
    return str;
  }
  return moment(str).format(pattern);
});
// =======
// Vue.use(Table);
// Vue.use(TableColumn);
// Vue.use(Popover);
// Vue.use(Pagination);
// Vue.use(OptionGroup);
// Vue.use(Link);
// Vue.use(Dialog);
// Vue.use(Loading.directive);
// Vue.use(Select);
// Vue.use(Option);
// Vue.use(Image);
// Vue.use(Icon);
// Vue.use(Tooltip);
// 导入富文本编辑器
import VueQuillEditor from 'vue-quill-editor';
import 'quill/dist/quill.core.css';
import 'quill/dist/quill.snow.css';
import 'quill/dist/quill.bubble.css';
Vue.use(VueQuillEditor);
// Vue.use(Drawer)
// Vue.use(DatePicker)
// Vue.use(PageHeader)
// Vue.use(Upload);
// Vue.use(Switch);
// Vue.use(Loading);
// Vue.use(Cascader);
// Vue.use(Tabs);
// Vue.use(TabPane);
// Vue.config.productionTip = false;
// Vue.prototype.$msgbox = MessageBox;
// Vue.prototype.$alert = MessageBox.alert;
// Vue.prototype.$confirm = MessageBox.confirm;
// Vue.prototype.$prompt = MessageBox.prompt;
// >>>>>>> 3ee5eb0557ca4fde8b7e53fffdac0cc7813e73f3


// 过滤时间
Vue.filter('dateFormat', function (formaTime) {
  let a = new Date(formaTime).getTime();
  const date = new Date(a);
  const Y = date.getFullYear() + "-";
  const M =
    (date.getMonth() + 1 < 10
      ? "0" + (date.getMonth() + 1)
      : date.getMonth() + 1) + "-";
  const D =
    (date.getDate() < 10 ? "0" + date.getDate() : date.getDate()) + " ";
  const h =
    (date.getHours() < 10 ? "0" + date.getHours() : date.getHours()) + ":";
  const m =
    date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
  const dateString = Y + M + D + h + m;
  return dateString;
})
new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
